package sample.services.listeners;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import sample.services.enums.MorphologyType;

import java.awt.*;

/**
 * Created by Wojciech on 01.11.2015.
 */
public class TransformsComboBoxChangeListener<T> implements ChangeListener<MorphologyType> {
    @Override
    public void changed(ObservableValue<? extends MorphologyType> observable, MorphologyType oldValue, MorphologyType newValue) {

    }
}
