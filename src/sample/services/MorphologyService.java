package sample.services;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import sample.services.enums.MorphologyType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;

/**
 * Created by ?ukasz on 10/29/15.
 */

public class MorphologyService {

    public BufferedImage transformFile(MorphologyType morphologyType, BufferedImage file, int value){

        switch(morphologyType){
            case Erosion:
                return erosion(file, value);
            case Dilatation:
                return dilatation(file, value);
            case Blur:
                return blur(file, value);
            case TopHat:
                return topHat(file, value);
            case BlackHat:
                return blackHat(file, value);
            case Opening:
                return blackHat(file, value);
            case Closing:
                return closing(file, value);
        }
        return null;
    }

    public BufferedImage erosion(BufferedImage file, int value)
    {
        String _tempName = "_erosion.jpg";
        try {
            if(file == null)
                return null;


          //  Mat source = Imgcodecs.imread(new File(file), Imgcodecs.CV_LOAD_IMAGE_COLOR);
            Mat source = MatImgConverter.img2Mat(file);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());

            int erosion_size = value;
            int dilation_size = value;

            Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2 * erosion_size + 1, 2 * erosion_size + 1));
            Imgproc.erode(source, destination, element);
            return MatImgConverter.mat2Img(destination);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return null;
        }
    }

    private BufferedImage dilatation(BufferedImage file, int value)
    {
        //todo FAKE for now
        String _tempName = "_dilatation.jpg";
        try {
            if(file == null)
                return null;
            Mat source = MatImgConverter.img2Mat(file);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());

            int erosion_size = value;
            int dilation_size = value;

            Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*dilation_size + 1, 2*dilation_size+1));
            Imgproc.dilate(source, destination, element);
            return MatImgConverter.mat2Img(destination);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return null;
        }
    }

    private BufferedImage blur(BufferedImage file, int value)
    {
        String _tempName = "_gausianBlur.jpg";
        try {
            if(file == null)
                return null;
            Mat source = MatImgConverter.img2Mat(file);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());

            int erosion_size = value;
            int dilation_size = value;

            Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*dilation_size + 1, 2*dilation_size+1));
            Imgproc.blur(source, destination, new Size(value, value));
            return MatImgConverter.mat2Img(destination);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return null;
        }
    }

    private BufferedImage topHat(BufferedImage file, int value)
    {
        String _tempName = "_topHat.jpg";
        try {
            if(file == null)
                return null;
            Mat source = MatImgConverter.img2Mat(file);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());

            int erosion_size = value;
            int dilation_size = value;

            Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*dilation_size + 1, 2*dilation_size+1));
            Imgproc.blur(source, destination, new Size(value, value));
            Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(value,value));

            Imgproc.morphologyEx(source, destination, Imgproc.MORPH_TOPHAT, kernel);
            return MatImgConverter.mat2Img(destination);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return null;
        }
    }

    private BufferedImage blackHat(BufferedImage file, int value)
    {
        String _tempName = "_topHat.jpg";
        try {
            if(file == null)
                return null;

            Mat source = MatImgConverter.img2Mat(file);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());

            int erosion_size = value;
            int dilation_size = value;

            Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*dilation_size + 1, 2*dilation_size+1));
            Imgproc.blur(source, destination, new Size(value, value));
            Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(value,value));

            Imgproc.morphologyEx(source, destination, Imgproc.MORPH_BLACKHAT, kernel);
            return MatImgConverter.mat2Img(destination);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return null;
        }

    }

    private BufferedImage opening(BufferedImage file, int value)
    {
        String _tempName = "_opening.jpg";
        try {
            if(file == null)
                return null;

            Mat source = MatImgConverter.img2Mat(file);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());

            int erosion_size = value;
            int dilation_size = value;

            Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*dilation_size + 1, 2*dilation_size+1));
            Imgproc.blur(source, destination, new Size(value, value));
            Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(value,value));

            Imgproc.morphologyEx(source, destination, Imgproc.MORPH_OPEN, kernel);
            return MatImgConverter.mat2Img(destination);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return null;
        }

    }
    private BufferedImage closing(BufferedImage file, int value)
    {
        String _tempName = "_closing.jpg";
        try {
            if(file == null)
                return null;

            Mat source = MatImgConverter.img2Mat(file);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());

            int erosion_size = value;
            int dilation_size = value;

            Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*dilation_size + 1, 2*dilation_size+1));
            Imgproc.blur(source, destination, new Size(value, value));
            Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(value,value));

            Imgproc.morphologyEx(source, destination, Imgproc.MORPH_CLOSE, kernel);
            return MatImgConverter.mat2Img(destination);
        } catch (Exception e) {
            System.out.println("error: " + e.getMessage());
            return null;
        }

    }


}
