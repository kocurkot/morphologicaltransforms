package sample.services.enums;

/**
 * Created by ?ukasz on 10/29/15.
 */
public enum MorphologyType {
    Erosion("Erosion"),
    Dilatation("Dilatation"),
    Blur("Blur"),
    TopHat("TopHat"),
    BlackHat("BlackHat"),
    Opening("Opening"),
    Closing("Closing");

    private final String text;

    /**
     * @param text
     */
    private MorphologyType(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
