package sample.DTOs;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Created by ?ukasz on 10/29/15.
 */
public class FileDTO {
    private File sourceFile;

    public void SetSourceFile(File file)
    {
        sourceFile = file;
    }

    public File getSourceFile() {
        return sourceFile;
    }
}
