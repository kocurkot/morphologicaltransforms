package sample.DTOs;

import java.awt.image.BufferedImage;

/**
 * Created by Wojciech on 01.11.2015.
 */
public class ImageDto {
    private BufferedImage bufferedImage;

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }
}
