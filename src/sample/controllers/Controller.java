package sample.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import javax.swing.*;

import javafx.embed.swing.SwingFXUtils;
import sample.DTOs.FileDTO;
import sample.DTOs.ImageDto;
import sample.services.MorphologyService;
import sample.services.enums.MorphologyType;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.control.ComboBox;
import sample.services.listeners.TransformsComboBoxChangeListener;

public class Controller implements Initializable {

    @FXML
    private Button chooseFileButton;
    @FXML
    private ImageView destinationImageView;
    @FXML
    private ImageView sourceImageView;
    @FXML
    private Slider valueSlider;
    @FXML
    private Label sliderValueLabel;
    @FXML
    private ComboBox<MorphologyType> transformsComboBox;


    private FileDTO fileDTO;
    private ImageDto imageDto;
    private MorphologyService morphologyService;

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        transformsComboBox.getItems().setAll(MorphologyType.values());
        transformsComboBox.autosize();
        transformsComboBox.setValue(MorphologyType.values()[0]);
        transformsComboBox.getSelectionModel().selectedItemProperty()
                .addListener(new TransformsComboBoxChangeListener<MorphologyType>());

        valueSlider.valueProperty().addListener((ov, old_val, new_val) -> {
            int value = new_val.intValue();

            sliderValueLabel.setText("Value: "+value);
            File file = fileDTO.getSourceFile();
            if(file == null)
                return;
            MorphologyType actualMorphologyType = transformsComboBox.getValue();
            BufferedImage bufferedImage = morphologyService.transformFile(actualMorphologyType,convertFileToBufferedImage(file),value);

            LoadFileToImageView(destinationImageView, bufferedImage);
        });

        sliderValueLabel.setVisible(false);
        valueSlider.setMin(1);
        valueSlider.setDisable(true);
    }

    public Controller() {
        this.fileDTO = new FileDTO();
        this.morphologyService = new MorphologyService();
    }

    public void chooseFileButtonAction(ActionEvent actionEvent) {

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter(
                "images", "*.jpg", "*.png"
        );
        fileChooser.getExtensionFilters().add(extensionFilter);
        fileChooser.setTitle("Open Resource File");

        fileDTO.SetSourceFile(fileChooser.showOpenDialog(chooseFileButton.getScene().getWindow()));

        valueSlider.setDisable(false);
        sliderValueLabel.setVisible(true);


        LoadFileToImageView(sourceImageView, fileDTO.getSourceFile());
    }

    public void saveImageToChoosenDestination(ActionEvent actionEvent) {
        // parent component of the dialog
        JFrame parentFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();

        fileChooser.setDialogTitle("Set destination path");
        int userSelection = fileChooser.showSaveDialog(parentFrame);

        if (userSelection == JFileChooser.APPROVE_OPTION) {

            File fileToSave = fileChooser.getSelectedFile();
            try {
                if (destinationImageView == null)
                    return;
                ImageIO.write(
                        SwingFXUtils.fromFXImage(
                                destinationImageView.snapshot(null, null), null
                        ),
                        "jpg",
                        fileToSave
                );
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private BufferedImage convertFileToBufferedImage(File file){
        try {
            BufferedImage in = ImageIO.read(file);
            return in;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void LoadFileToImageView(ImageView imgView, File file){
            BufferedImage bufferedImage = convertFileToBufferedImage(file);
            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
            imgView.setImage(image);
    }

    private void LoadFileToImageView(ImageView imgView, BufferedImage bufferedImage){
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        imgView.setImage(image);
    }
}
