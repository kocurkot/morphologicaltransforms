package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.opencv.core.Core;

public class Main  extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        Image image = new Image("file:icon.png");

        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Morphological transforms");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.setResizable(true);
        primaryStage.getIcons().add(image);

//        BorderPane borderPaneRoot = (BorderPane)root;
//        borderPaneRoot.setTop(borderPaneRoot.getTop().ad);

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
